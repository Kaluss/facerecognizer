#ifndef MAINWINDOW_H
#define MAINWINDOW_H

//Qt SDK
#include <QMainWindow>
#include <QLabel>

//My SDK
#include "centralviewwidget.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    void closeEvent( QCloseEvent *event ) override;

    CentralViewWidget* m_centralView;
};

#endif // MAINWINDOW_H
