; This examples demonstrates how libusb's drivers
; can be installed automatically along with your application using an installer.
;
; Requirements: Inno Setup (http://www.jrsoftware.org/isdl.php)
;
; To use this script, do the following:
; - generate a setup package using inf-wizard and save the generated files to
;   "this folder\driver"
; - in this script replace <your_inf_file.inf> with the name of your .inf file
; - customize other settings (strings)
; - open this script with Inno Setup
; - compile and run

[Setup]
AppName = FaceRecognizer
AppVerName = FaceRecognizer 1.1.0
AppPublisher = AGH im. S. Staszica
AppPublisherURL = http://inventexpress.eu
AppVersion = 0.1.10.1
DefaultDirName = C:\AGH\FaceRecognizer
DefaultGroupName = FaceRecognizer
Compression = lzma
SolidCompression = yes
AppCopyright = Copyright 2014 AGH im. S. Staszica
Uninstallable = yes
;InfoBeforeFile="read_before_install.txt"
LicenseFile="license.txt"
; Image used on install wizard
;WizardImageFile="C:\My Documents\Tal Systems\Corporate\Artwork\Tal_lg_120x60.bmp"
;SetupIconFile="add.png"

; Win2000 or higher
MinVersion = 5

; This installation requires admin priveledges.  This is needed to install
; drivers on windows vista and later.
PrivilegesRequired = admin

; "ArchitecturesInstallIn64BitMode=x64 ia64" requests that the install
; be done in "64-bit mode" on x64 & Itanium, meaning it should use the
; native 64-bit Program Files directory and the 64-bit view of the
; registry. On all other architectures it will install in "32-bit mode".
ArchitecturesInstallIn64BitMode=x64 ia64

[Tasks] 
; NOTE: The following entry contains English phrases ("Create a desktop icon" and "Additional icons"). You are free to translate them into another language if required. 
Name: "desktopicon"; Description: "Utw�rz &ikon� na pulpicie"; GroupDescription: "Additional icons:"

; Inno pascal functions for determining the processor type.
; you can use these to use (in an inno "check" parameter for example) to
; customize the installation depending on the architecture. 
[Code]
function IsX64: Boolean;
begin
  Result := Is64BitInstallMode and (ProcessorArchitecture = paX64);
end;

function IsI64: Boolean;
begin
  Result := Is64BitInstallMode and (ProcessorArchitecture = paIA64);
end;

function IsX86: Boolean;
begin
  Result := not IsX64 and not IsI64;
end;

function Is64: Boolean;
begin
  Result := IsX64 or IsI64;
end;

[Dirs]
Name: "{app}\imageformats";
;Name: "{app}\data";
;Name: "{app}\tmp";
;Name: "{app}\inwentaryzacje";
;Name: "{app}\gfx";
;Name: "{app}\driver"; Flags: deleteafterinstall;
;Name: "{app}\driver\amd64"; Flags: deleteafterinstall;
;;Name: "{app}\driver\ia64"; Flags: deleteafterinstall;
;Name: "{app}\driver\x86"; Flags: deleteafterinstall;

[Files]
; copy your libusb-win32 setup package to the App folder
;Source: "driver\*"; Excludes: "*.exe"; Flags: deleteafterinstall recursesubdirs; DestDir: "{app}\driver"

; also copy the native (32bit or 64 bit) libusb0.dll to the 
; system folder so that rundll32.exe will find it
;Source: "driver\x86\libusb0_x86.dll"; DestName: "libusb0.dll"; DestDir: "{sys}"; Flags: uninsneveruninstall replacesameversion restartreplace promptifolder; Check: IsX86;
;Source: "driver\amd64\libusb0.dll"; DestDir: "{sys}"; Flags: uninsneveruninstall replacesameversion restartreplace promptifolder; Check: IsX64;
;Source: "driver\ia64\libusb0.dll"; DestDir: {sys}; Flags: uninsneveruninstall replacesameversion restartreplace promptifolder; Check: IsI64;

Source: "FaceRecognizer.exe"; DestDir: "{app}";
Source: "libstdc++-6.dll"; DestDir: "{app}";
Source: "qjpeg4.dll"; DestDir: "{app}\imageformats";
;Source: "QtNetwork4.dll"; DestDir: "{app}";
;Source: "QtGui4.dll"; DestDir: "{app}";
;Source: "libgcc_s_dw2-1.dll"; DestDir: "{app}";
Source: "ikona.ico"; DestDir: "{app}";

;Source: "files\*"; DestDir: "{app}\files"; Flags: onlyifdoesntexist;
;Source: "version.dat"; DestDir: "{app}"; Flags: onlyifdoesntexist;
;Source: "gfx\*"; DestDir: "{app}\gfx";

[Icons]
Name: "{group}\Odinstaluj FaceRecognizer"; Filename: "{uninstallexe}"
Name: "{group}\FaceRecognizer"; Filename: "{app}\FaceRecognizer.exe"; IconFilename: "{app}\ikona.ico"
Name: "{userdesktop}\FaceRecognizer"; Filename: "{app}\FaceRecognizer.exe"; Tasks: desktopicon; IconFilename: "{app}\ikona.ico"

[Run]
; touch the HID .inf file to break its digital signature
; this is only required if the device is a mouse or a keyboard !!
;Filename: "rundll32"; Parameters: "libusb0.dll,usb_touch_inf_file_np_rundll {win}\inf\input.inf"

; invoke libusb's DLL to install the .inf file
;Filename: "rundll32"; Parameters: "libusb0.dll,usb_install_driver_np_rundll {app}\driver\driver.inf"; StatusMsg: "Instalacja sterownika (mo�e zaj�� chwil�) ..."
