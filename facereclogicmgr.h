#ifndef FACERECLOGICMGR_H
#define FACERECLOGICMGR_H

//Qt SDK
#include <QVector>
#include <QString>
#include <QObject>
#include <QLabel>

//class prototypes
class CentralViewWidget;

class FaceRecLogicMgr : public QObject
{
   Q_OBJECT
public:
   explicit FaceRecLogicMgr( CentralViewWidget *parent );

   void addPic( const QString& picPath );

   void clearPicList();

   void setPreview( );

   void setNoPics();

   void movePicUp();
   void movePicDown();

   void runFaceRecognizerAlgorithm();

   QPixmap getWorkingImage();

protected:
   QPixmap getProperPixmap( const QString& path, size_t width, size_t height );

signals:

public slots:

private:
   QVector< QString > m_picTab;         ///< working pics tab
   CentralViewWidget  *m_centralWidget; ///< pointer to CentralViewWidget object
};
#endif // FACERECLOGICMGR_H
