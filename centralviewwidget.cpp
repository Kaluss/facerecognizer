//Qt SDK
#include <QFileDialog>
#include <QStandardPaths>
#include <QMessageBox>

//My SDK
#include "centralviewwidget.h"
#include "mainwindow.h"

CentralViewWidget::CentralViewWidget( MainWindow *parent ) : QWidget( parent ),
                                                             m_mainWindow( parent )
{
   //logic object initialization
   this->m_logicManager.reset( new FaceRecLogicMgr( this ) );

   //layouts initialization
   this->m_hbox_main    = new QHBoxLayout();
   this->m_vbox_preview = new QVBoxLayout();
   this->m_vbox_central = new QVBoxLayout();
   this->m_vbox_menu    = new QVBoxLayout();

   //creating layout
   this->m_button_up = new QPushButton( "UP", this );
   this->m_button_up->setMinimumHeight( 30 );
   this->m_vbox_preview->addWidget( this->m_button_up, 1, Qt::AlignBottom );

   this->m_label_previewPicUp = new QLabel( this );
   this->m_label_previewPicUp->setStyleSheet( "border: 3px solid  #ff0000" );
   this->m_label_previewPicUp->setFixedSize( 150, 150 );
   this->m_label_previewPicUp->setAlignment( Qt::AlignCenter );
   this->m_vbox_preview->addWidget( this->m_label_previewPicUp, 1, Qt::AlignHCenter );

   this->m_label_previewPicMiddle = new QLabel( this );
   this->m_label_previewPicMiddle->setStyleSheet( "border: 1px solid" );
   this->m_label_previewPicMiddle->setFixedSize( 150, 150 );
   this->m_label_previewPicMiddle->setAlignment( Qt::AlignCenter );
   this->m_vbox_preview->addWidget( this->m_label_previewPicMiddle, 1, Qt::AlignHCenter );

   this->m_label_previewPicDown = new QLabel( this );
   this->m_label_previewPicDown->setStyleSheet( "border: 1px solid" );
   this->m_label_previewPicDown->setFixedSize( 150, 150 );
   this->m_label_previewPicDown->setAlignment( Qt::AlignCenter );
   this->m_vbox_preview->addWidget( this->m_label_previewPicDown, 1, Qt::AlignHCenter );

   this->m_button_down = new QPushButton( "DOWN", this );
   this->m_button_down->setMinimumHeight( 30 );
   this->m_vbox_preview->addWidget( this->m_button_down, 1, Qt::AlignTop );

   this->m_hbox_main->addLayout( this->m_vbox_preview, 1 );

   this->m_label_pic = new QLabel( this );
   this->m_label_pic->setPixmap( QPixmap( ":/gfx/gfx/logoPic.png" ) );
   this->m_vbox_central->addWidget( this->m_label_pic, 1, Qt::AlignHCenter );

   this->m_label_workingPic       = new QLabel( this );
   this->m_scrollarea_selectedPic = new QScrollArea( this );
   this->m_scrollarea_selectedPic->setWidget( this->m_label_workingPic );
   this->m_scrollarea_selectedPic->setAlignment( Qt::AlignCenter );
   this->m_vbox_central->addWidget( this->m_scrollarea_selectedPic, 1, Qt::AlignHCenter );

   this->m_button_selectImage = new QPushButton( "SELECT IMAGE", this );
   this->m_button_selectImage->setMinimumSize( 400, 30 );
   this->m_button_selectImage->setFont( QFont( "Arial", 15, QFont::Bold ) );
   this->m_vbox_central->addWidget( this->m_button_selectImage, 1, Qt::AlignHCenter );

   this->m_vbox_central->addSpacing( 100 );

   this->m_hbox_main->addLayout( this->m_vbox_central, 1 );

   this->m_vbox_menu->addSpacing( 100 );

   this->m_label_menuTitle = new QLabel( this );
   this->m_label_menuTitle->setFont( QFont( "Arial", 20, QFont::Bold ) );
   this->m_label_menuTitle->setText( "Main Menu" );
   this->m_vbox_menu->addWidget( this->m_label_menuTitle, 1, Qt::AlignHCenter );

   this->m_vbox_menu->addSpacing( 50 );

   this->m_button_loadFiles = new QPushButton( "Load image files", this );
   this->m_button_loadFiles->setMinimumHeight( 30 );
   this->m_vbox_menu->addWidget( this->m_button_loadFiles, 1, Qt::AlignBottom );

   this->m_button_checkFace = new QPushButton( "Find face on pic", this );
   this->m_button_checkFace->setMinimumHeight( 30 );
   this->m_vbox_menu->addWidget( this->m_button_checkFace, 1, Qt::AlignBottom  );

   this->m_button_clear = new QPushButton( "Clear pic list", this );
   this->m_button_clear->setMinimumHeight( 30 );
   this->m_vbox_menu->addWidget( this->m_button_clear, 1, Qt::AlignBottom  );

   this->m_button_exit = new QPushButton( "Exit", this );
   this->m_button_exit->setMinimumHeight( 30 );
   this->m_vbox_menu->addWidget( this->m_button_exit, 1, Qt::AlignBottom  );

   this->m_vbox_menu->addSpacing( 150 );

   this->m_label_aghPic = new QLabel( this );
   this->m_label_aghPic->setPixmap( QPixmap( ":/gfx/gfx/aghPic.png" ) );
   this->m_vbox_menu->addWidget( this->m_label_aghPic, 1, Qt::AlignHCenter );

   this->m_vbox_menu->addSpacing( 50 );

   this->m_hbox_main->addLayout( this->m_vbox_menu, 1 );

   this->setLayout( this->m_hbox_main );

   //conecting signals
   QObject::connect( this->m_button_exit, SIGNAL( clicked() ), this->m_mainWindow, SLOT( close() ) );
   QObject::connect( this->m_button_loadFiles, SIGNAL( clicked() ), this, SLOT( loadFilesSlot() ) );
   QObject::connect( this->m_button_clear, SIGNAL( clicked() ), this, SLOT( clearPicListSlot() ) );
   QObject::connect( this->m_button_selectImage, SIGNAL( clicked() ), this, SLOT( setMainImageSlot() ) );
   QObject::connect( this->m_button_up, SIGNAL( clicked() ), this, SLOT( selectImageDownSlot()  ) );
   QObject::connect( this->m_button_down, SIGNAL( clicked() ), this, SLOT( selectImageUpSlot() ) );
   QObject::connect( this->m_button_checkFace, SIGNAL( clicked() ), this, SLOT( runFaceRecognizerSlot() ) );

   this->m_previewTab.push_back( this->m_label_previewPicUp );
   this->m_previewTab.push_back( this->m_label_previewPicMiddle );
   this->m_previewTab.push_back( this->m_label_previewPicDown );

   this->m_logicManager->setNoPics( );
}


void CentralViewWidget::loadFilesSlot()
{
   QStringList filesPaths = QFileDialog::getOpenFileNames( this, "Load images", QStandardPaths::writableLocation( QStandardPaths::DesktopLocation ), " jpg png (*.jpg *.png)" );

   if( filesPaths.isEmpty() )
   {
      return;
   }

   foreach( QString path, filesPaths )
   {
      this->m_logicManager->addPic( path );
   }

   this->m_logicManager->setPreview();
}


void CentralViewWidget::clearPicListSlot()
{
   this->m_logicManager->clearPicList();
   this->m_label_workingPic->setPixmap( QPixmap( "" ) );
   this->m_label_workingPic->adjustSize();
}


void CentralViewWidget::setMainImageSlot()
{
   this->m_label_workingPic->setPixmap( this->m_logicManager->getWorkingImage() );

   m_label_workingPic->adjustSize();
}


void CentralViewWidget::selectImageUpSlot()
{
   this->m_logicManager->movePicDown();
}


void CentralViewWidget::selectImageDownSlot()
{
   this->m_logicManager->movePicUp();
}


void CentralViewWidget::runFaceRecognizerSlot()
{
   QMessageBox::information( this, "Info", "Tutaj wykonywać się będzie algorytm wykrywania twarzy...\n Po ustaleniach z Panem doktorem!" );
}


void CentralViewWidget::resizeEvent( QResizeEvent *event )
{
   this->m_scrollarea_selectedPic->setFixedSize( this->width() / 1.7, this->height() / 1.7 );
   QWidget::resizeEvent( event );
}
