//My SDK
#include "facereclogicmgr.h"
#include "centralviewwidget.h"

//Qt SDK
#include <QMessageBox>

FaceRecLogicMgr::FaceRecLogicMgr( CentralViewWidget *parent ) : QObject( parent ),
                                                                m_centralWidget( parent )
{
}


void FaceRecLogicMgr::addPic( const QString& picPath )
{
   if( picPath.isEmpty() )
   {
      return;
   }
   this->m_picTab.push_back( picPath );
}


void FaceRecLogicMgr::clearPicList()
{
   this->m_picTab.clear();
   this->setNoPics();
}


void FaceRecLogicMgr::setPreview()
{
   this->setNoPics();

   if( this->m_picTab.isEmpty() )
   {
      return;
   }
   for( int picIndex = 0 ; picIndex < this->m_picTab.size() && picIndex < this->m_centralWidget->m_previewTab.size() ; ++picIndex )
   {
      this->m_centralWidget->m_previewTab[ picIndex ]->setPixmap( getProperPixmap( this->m_picTab[ picIndex ],
                                                                                   this->m_centralWidget->m_previewTab[ picIndex ]->width(),
                                                                                   this->m_centralWidget->m_previewTab[ picIndex ]->height() ) );
   }
}


void FaceRecLogicMgr::setNoPics()
{
   foreach( QLabel * label, this->m_centralWidget->m_previewTab )
   {
      label->setPixmap( QPixmap( ":/gfx/gfx/NoImage.png" ) );
   }
}


void FaceRecLogicMgr::movePicDown()
{
   if( this->m_picTab.size() < 2 )
   {
      return;
   }

   QString lastSelectedPic( this->m_picTab[ this->m_picTab.size() - 1 ] );

   QVector< QString > tmpPicTab( this->m_picTab );

   this->m_picTab.clear();

   this->m_picTab.push_back( lastSelectedPic );

   for( int picIndex = 0 ; picIndex < tmpPicTab.size() - 1 ; ++picIndex )
   {
      this->m_picTab.push_back( tmpPicTab[ picIndex ] );
   }
   this->setPreview();
}


void FaceRecLogicMgr::movePicUp()
{
   if( this->m_picTab.size() < 2 )
   {
      return;
   }

   QString lastSelectedPic( *this->m_picTab.begin() );

   QVector< QString > tmpPicTab( this->m_picTab );

   this->m_picTab.clear();

   for( int picIndex = 1 ; picIndex < tmpPicTab.size() ; ++picIndex )
   {
      this->m_picTab.push_back( tmpPicTab[ picIndex ] );
   }
   this->m_picTab.push_back( lastSelectedPic );

   this->setPreview();
}


void FaceRecLogicMgr::runFaceRecognizerAlgorithm()
{
}


QPixmap FaceRecLogicMgr::getWorkingImage()
{
   if( this->m_picTab.isEmpty() )
   {
      QMessageBox::information( this->m_centralWidget, "Info", "No images loaded!" );
      return QPixmap( "" );
   }
   return QPixmap( *this->m_picTab.begin() );
}


QPixmap FaceRecLogicMgr::getProperPixmap( const QString& path, size_t width, size_t height )
{
   QPixmap pic( path );

   if( pic.width() > pic.height() )
   {
      return pic.scaledToWidth( width, Qt::TransformationMode::SmoothTransformation );
   }
   else
   {
      return pic.scaledToHeight( height, Qt::TransformationMode::SmoothTransformation );
   }
}
