#-------------------------------------------------
#
# Project created by QtCreator 2014-11-24T23:51:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FaceRecognizer
TEMPLATE = app

CONFIG += c++11


SOURCES += main.cpp\
        mainwindow.cpp \
    centralviewwidget.cpp \
    facereclogicmgr.cpp

HEADERS  += mainwindow.h \
    centralviewwidget.h \
    facereclogicmgr.h

RESOURCES += \
    gfxResources.qrc

RC_FILE = my.rc
