//Qt SDK
#include <QCloseEvent>
#include <QMessageBox>
#include <QApplication>

//My SDK
#include "mainwindow.h"

MainWindow::MainWindow( QWidget *parent )
   : QMainWindow( parent )
{
   this->setWindowState( Qt::WindowMaximized );
   this->setWindowFlags( Qt::WindowCloseButtonHint | Qt::WindowMinMaxButtonsHint );

   this->m_centralView = new CentralViewWidget( this );
   this->setCentralWidget( this->m_centralView );
}


MainWindow::~MainWindow()
{
}


void MainWindow::closeEvent( QCloseEvent *event )
{
   event->ignore();

   if( QMessageBox::question( this, "Leave question", "Close FaceRecognizer ?" ) == QMessageBox::Yes )
   {
      QApplication::quit();
   }
}
