#ifndef CENTRALVIEWWIDGET_H
#define CENTRALVIEWWIDGET_H

//Qt SDK
#include <QLabel>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QScrollArea>
#include <QScopedPointer>

//My SDK
#include "facereclogicmgr.h"

//class prototypes
class MainWindow;

class CentralViewWidget : public QWidget
{
   Q_OBJECT

   friend class FaceRecLogicMgr;
public:
   explicit CentralViewWidget( MainWindow *parent );
signals:

public slots:
private slots:
   void loadFilesSlot();
   void clearPicListSlot();
   void setMainImageSlot();
   void selectImageDownSlot();
   void selectImageUpSlot();

   void runFaceRecognizerSlot();

private:
   void resizeEvent( QResizeEvent *event ) override;

   MainWindow *m_mainWindow;                         ///< pointer to main window object
   QScopedPointer< FaceRecLogicMgr > m_logicManager; ///< manages all app calculation
   QVector< QLabel * >               m_previewTab;   ///< preview places tab

   //layout stuff
   QLabel      *m_label_pic, *m_label_workingPic, *m_label_aghPic;
   QLabel      *m_label_menuTitle, *m_label_previewPicUp, *m_label_previewPicMiddle, *m_label_previewPicDown;
   QHBoxLayout *m_hbox_main;
   QVBoxLayout *m_vbox_central, *m_vbox_menu, *m_vbox_preview;
   QPushButton *m_button_loadFiles, *m_button_checkFace, *m_button_exit;
   QPushButton *m_button_clear, *m_button_up, *m_button_down, *m_button_selectImage;
   QScrollArea *m_scrollarea_selectedPic;
};
#endif // CENTRALVIEWWIDGET_H
