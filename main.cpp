//Qt SDK
#include <QApplication>
#include <QSplashScreen>
#include <QThread>

//My SDK
#include "mainwindow.h"

int main( int argc, char *argv[] )
{
   QApplication a( argc, argv );

   //operation splash screen

   QPixmap spl00( ":/splash/gfx/splash00.png" );
   QPixmap spl01( ":/splash/gfx/splash01.png" );
   QPixmap spl02( ":/splash/gfx/splash02.png" );
   QPixmap spl03( ":/splash/gfx/splash03.png" );
   QPixmap spl04( ":/splash/gfx/splash04.png" );
   QPixmap spl05( ":/splash/gfx/splash05.png" );
   QPixmap spl06( ":/splash/gfx/splash06.png" );
   QPixmap spl07( ":/splash/gfx/splash07.png" );
   QPixmap spl08( ":/splash/gfx/splash08.png" );
   QPixmap spl09( ":/splash/gfx/splash09.png" );
   QPixmap spl10( ":/splash/gfx/splash10.png" );

   QPixmap splashTab[] =
   {
      spl00, spl01, spl02, spl03, spl04, spl05, spl06, spl07, spl08, spl09, spl10,
      spl09, spl08, spl07, spl06, spl05, spl04, spl03, spl02, spl01
   };

   QSplashScreen *splash = new QSplashScreen( splashTab[ 0 ] );

   splash->show();

   for( size_t stRound = 0 ; stRound < 5 ; ++stRound )
   {
      for( size_t picNumber = 0 ; picNumber < sizeof( splashTab ) / sizeof( splashTab[ 0 ] ) ; ++picNumber )
      {
         splash->setPixmap( splashTab[ picNumber ] );
         QThread::msleep( 75 );
      }
   }
   delete splash;

   //the end of operation splash screen

   MainWindow w;

   w.show();

   return a.exec();
}
